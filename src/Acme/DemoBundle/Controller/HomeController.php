<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\DemoBundle\Entity\Hotel;
use Acme\DemoBundle\Entity\Room;
use Acme\DemoBundle\Entity\Reservation;
use Acme\DemoBundle\Entity\Customer;

class HomeController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeDemoBundle:Test:home.html.twig');
    }

    public function listAction()
    {
        $_hotel = $this->getDoctrine()->getRepository('AcmeDemoBundle:Hotel');
        $data['hotels'] = $_hotel->findBy(array(), array('stars' => 'DESC', 'name' => 'ASC'));

        return $this->render('AcmeDemoBundle:Test:hotels.html.twig', $data);
    }

    public function reservationAction($id)
    {
        $_hotel = $this->getDoctrine()->getRepository('AcmeDemoBundle:Hotel');
        $_room = $this->getDoctrine()->getRepository('AcmeDemoBundle:Room');
        $_reservation = $this->getDoctrine()->getRepository('AcmeDemoBundle:Reservation');
        $_customer = $this->getDoctrine()->getRepository('AcmeDemoBundle:Customer');
        $data['hotel'] = $_hotel->find($id);
        $data['reservations'] = array();
        $rooms = $_room->findBy(array('hotel' => $id));

        //Recorremos listado de habitaciobes
        foreach ($rooms as $room) {
            $reservations = $_reservation->findBy(array('idRoom' => $room->getId()));

            //Recorremos el listado de reservas
            foreach ($reservations as $reservation) {
                $customer = $_customer->find($reservation->getIdCustomer());
                $data['reservations'][] = array(
                    'room' => $room->getNumber(),
                    'checkin' => $reservation->getCheckIn()->format('Y-m-d'),
                    'checkout' => $reservation->getCheckOut()->format('Y-m-d'),
                    'customer' => $customer->getName(),
                    'customer_age' => $customer->getAge()
                );
            }
        }

        return $this->render('AcmeDemoBundle:Test:reservation.html.twig', $data);
    }
}
