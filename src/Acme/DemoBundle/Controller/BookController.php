<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\DemoBundle\Entity\Hotel;
use Acme\DemoBundle\Entity\Room;
use Acme\DemoBundle\Entity\Reservation;
use Acme\DemoBundle\Entity\Customer;
use Symfony\Component\HttpFoundation\Request;

class BookController extends Controller
{
    public function bookAction(Request $request)
    {
        $_hotel = $this->getDoctrine()->getRepository('AcmeDemoBundle:Hotel');
        $_room = $this->getDoctrine()->getRepository('AcmeDemoBundle:Room');
        $_reservation = $this->getDoctrine()->getRepository('AcmeDemoBundle:Reservation');
        $hotels = $_hotel->findBy(array(), array('stars' => 'DESC', 'name' => 'ASC'));
        $choices = array();

        //Creamos un listado de opciones para el formulario
        foreach ($hotels as $hotel) {
            $choices[$hotel->getId()] = $hotel->getName().' ('.$hotel->getCity().') - '.$hotel->getStars().' stars';
        }

        $form = $this->createFormBuilder()
            ->add('id_customer', 'integer', array('label' => 'Your ID'))
            ->add('customer_name', 'text', array('label' => 'Your name'))
            ->add('checkin', 'date')
            ->add('checkout', 'date')
            ->add('hotel', 'choice', array('choices' => $choices))
            ->getForm();

        //Comprobamos si se ha recibido un formulario
        if ($request->isMethod('POST')) {
            $form->bind($request);
            $_customer = $this->getDoctrine()->getRepository('AcmeDemoBundle:Customer');
            //Validamos el formulario
            $check = self::validate($form->getData(), $_customer, $_reservation, $_room);

            if (!$check['error']) {
                $data['success'] = $check['validate'];
            } else {
                $data['error'] = $check['error'];
            }
        }

        $data['form'] = $form->createView();

        return $this->render('AcmeDemoBundle:Test:book.html.twig', $data);
    }

    function validate($data, $_customer, $_reservation, $_room)
    {
        $error = false;
        $message = '';

        if (empty($data['id_customer'])) {
            $error = 'Error, the id is required.';
        }
        if (!$error && empty($data['customer_name'])) {
            $error = 'Error, the name is required.';
        }
        if (!$error && empty($data['checkin'])) {
            $error = 'Error, check-in is required.';
        } else if (!$error && strtotime($data['checkin']->format('Y-m-d')) < strtotime(date('Y-m-d'))) {
            $error = 'Error, check-in date is invalid.';
        }
        if (!$error && empty($data['checkout'])) {
            $error = 'Error, check-out is required.';
        } else if (!$error && strtotime($data['checkout']->format('Y-m-d')) < strtotime(date('Y-m-d'))
            || strtotime($data['checkout']->format('Y-m-d')) < strtotime($data['checkin']->format('Y-m-d'))) {
            $error = 'Error, check-out date is invalid.';
        }
        if (!$error && empty($data['hotel'])) {
            $error = 'Error, hotel is required.';
        }

        if (!$error) {
            //Comprobamos los datos del cliente
            $customer = $_customer->findOneBy(
                array('id' => $data['id_customer'], 'name' => $data['customer_name'])
            );

            if (!$customer) {
                $error = 'Error, invalid name or Id';
            } else {
                //Comprobamos que el cliente no haya alquilado una habitación a partir de la fecha actual
                $em = $this->getDoctrine()->getManager();
                $query = $em->createQuery(
                    'SELECT r FROM AcmeDemoBundle:Reservation r WHERE r.idCustomer = :customer AND r.checkOut >= :now')
                    ->setParameter('customer', $data['id_customer'])
                    ->setParameter('now', date('Y-m-d'));
                $book = $query->getResult();

                if (count($book)) {
                    $error = 'Error, You have booked one room.';
                } else {
                    //Comprobamos que el hotel seleccionado tenga habitaciones disponibles
                    $rooms = $_room->findBy(array('hotel' => $data['hotel']), array());

                    if (count($rooms)) {
                        $booked = array();
                        $the_room = false;

                        foreach ($rooms as $room) {
                            $query = $em->createQuery(
                                'SELECT r FROM AcmeDemoBundle:Reservation r WHERE r.idRoom = :room AND r.checkOut >= :checkin')
                                ->setParameter('room', $room->getId())
                                ->setParameter('checkin', $data['checkin']);
                            $booked = $query->getResult();

                            if (!count($booked)) {
                                $the_room = array('id' => $room->getId(), 'number' => $room->getNumber());
                                break;
                            }
                        }

                        if ($the_room) {
                            //Reservamos la habitación
                            $reservation = new Reservation();
                            $reservation->setIdRoom($the_room['id']);
                            $reservation->setIdCustomer($data['id_customer']);
                            $reservation->setCheckIn($data['checkin']);
                            $reservation->setCheckOut($data['checkout']);
                            $em->persist($reservation);
                            $em->flush();

                            if (empty($reservation->getId())) {
                                $error = 'Error, one error ocurred.';
                            } else {
                                $message = 'You have book the room '.$the_room['number'].'. Thank you!';
                            }
                        } else {
                            $error = 'Error, not found available rooms.';
                        }
                    } else {
                        $error = 'Error, not found available rooms.';
                    }
                }
            }
        }

        return array('error' => $error, 'validate' => $message);
    }
}
