/**
 * Funciones generales
 * @author  Christian Gil
 */
(function($)
{
    $(document).ready(function()
    {
        $('#book-form .send').click(function(e)
        {
            e.preventDefault();
            $('form p.error').remove();

            var id_customer = $('#form_id_customer').val();
            var customer = $('#form_customer_name').val();
            var checkin = new Date($('#form_checkin_year').val(), $('#form_checkin_month').val(), $('#form_checkin_day').val());
            var checkout = new Date($('#form_checkout_year').val(), $('#form_checkout_month').val(), $('#form_hotel').val());
            var error = false;
            var date = new Date();
            var now = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            //Comprobamos si el campo ID es mayor que 0
            if (parseInt(id_customer) <= 0) {
                $('#form_id_customer').after('<p class="error">Error, is not a valid ID.</p>');
                error = true;
            }
            //Comprobamos si se ha introducido algún carácter
            if (!customer.length) {
                $('#form_id_customer').after('<p class="error">Error, is not a valid ID.</p>');
                error = true;
            }
            //Comprobamos si la fecha de entrada es menor a la fecha actual
            if (checkin < now) {
                $('#form_checkin_day').after('<p class="error">Error, is not a valid Date.</p>');
                error = true;
            }
            //Comprobamos si la fecha de salida es menor a la fecha actual o si es menor que la fecha de entrada
            if (checkout < now || checkout < checkin) {
                console.log(checkout < checkin);
                $('#form_checkout_day').after('<p class="error">Error, is not a valid Date.</p>');
                error = true;
            }
            //Si no existe ningún error, enviamos el formulario
            if (!error) {
                $('#book-form').submit();
            }
        });
    });
})(jQuery);